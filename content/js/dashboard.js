/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9483760975193931, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999258754113914, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9103700421005982, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.8256799717414341, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.8569715142428785, 500, 1500, "me"], "isController": false}, {"data": [0.9475535918834888, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.8620840630472855, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.903539615564932, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58655, 0, 0.0, 256.10277043730287, 11, 56949, 50.0, 289.0, 919.9500000000007, 2588.870000000021, 189.6845005562311, 572.0784945237918, 228.64428944005803], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 35, 0, 0.0, 44029.42857142857, 20998, 56949, 46195.0, 56002.4, 56705.0, 56949.0, 0.11318728939079366, 0.07151579710531593, 0.14004716372864803], "isController": false}, {"data": ["getLatestMobileVersion", 33727, 0, 0.0, 44.05402200017809, 11, 1250, 25.0, 93.0, 149.0, 224.0, 112.46944580611384, 75.23590857147263, 82.92424959337495], "isController": false}, {"data": ["findAllConfigByCategory", 4513, 0, 0.0, 331.1914469310879, 37, 2916, 177.0, 848.7999999999997, 1423.1000000000013, 1931.0199999999977, 15.043082615289746, 17.011611004399928, 19.538378787436876], "isController": false}, {"data": ["getNotifications", 2831, 0, 0.0, 528.2151183327446, 55, 3723, 220.0, 1816.6000000000004, 2255.0, 2838.7599999999948, 9.435157041540021, 80.28176103802558, 10.494769404603597], "isController": false}, {"data": ["getHomefeed", 75, 0, 0.0, 20315.413333333338, 8756, 23491, 21505.0, 22670.4, 23386.6, 23491.0, 0.2452054167511263, 4.2884607505819545, 1.2269849174148157], "isController": false}, {"data": ["me", 3335, 0, 0.0, 448.31904047976036, 66, 3206, 212.0, 1383.4, 1912.199999999999, 2301.64, 11.115851504223023, 14.615471167531048, 35.22552551875363], "isController": false}, {"data": ["getAllClassInfo", 6111, 0, 0.0, 244.45459008345568, 19, 2456, 157.0, 433.40000000000055, 1011.0, 1609.5200000000004, 20.371290181711508, 11.777152136301966, 52.30090028097613], "isController": false}, {"data": ["findAllChildrenByParent", 3426, 0, 0.0, 436.4597197898426, 65, 3492, 199.0, 1356.9000000000005, 1912.8999999999978, 2406.9500000000003, 11.419923867174218, 12.825109811767922, 18.24511274091506], "isController": false}, {"data": ["findAllSchoolConfig", 4266, 0, 0.0, 350.2632442569152, 50, 3052, 192.0, 919.0, 1458.0, 1999.33, 14.220900657041613, 310.1378451885286, 10.429586321717043], "isController": false}, {"data": ["getChildCheckInCheckOut", 336, 0, 0.0, 4472.10714285714, 2597, 10009, 4377.5, 5534.1, 6112.999999999998, 9108.89, 1.1110596731632794, 62.868183185963616, 5.112610527290403], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58655, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
